from src import Particle, Universe, random_tuple
from random import choice

if __name__ == "__main__":
    u = Universe(edges="bouncy", friction=0.9, gravity=80, electromagnetism=1)

    proton =   {"mass":    1.0,
                "charge":  1.0,
                "color":   (255, 80, 80)}
    neutron =  {"mass":    1.0,
                "charge":  0.0,
                "color":   (179, 179, 179)}
    electron = {"mass":    0.0001,
                "charge": -1.0,
                "color":   (0, 153, 255)}

    for p in [proton, neutron, electron]:
        for _ in range(4):
            u.add_particle(pos=random_tuple(500), vel=random_tuple(500), **p)

    u.loop()


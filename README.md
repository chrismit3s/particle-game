# particle-game

A game made of particles. Each particle has mass and charge, and similar to our world, mass attracts mass and (positive) charge repells (positive) charge. Mass is also a metric for how resistant a particle is to acceleration.

A universe is just made up of particles that behave according to the simple rules stated above.